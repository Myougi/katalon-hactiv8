import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.comment('Menambahkan Produk ke dalam cart belanja')

Mobile.tap(findTestObject('Solocomerce/icon_Search'), 5)

Mobile.setText(findTestObject('Solocomerce/txt_search_item'), item_name, 5)

Mobile.tap(findTestObject('Solocomerce/frm_item'), 5)

Mobile.tap(findTestObject('Solocomerce/Page - Detail Product/btn_Add To Cart'), 5)

Mobile.setText(findTestObject('Solocomerce/Page - Detail Product/txt_Input Number of Order'), number_of_order, 5)

Mobile.tap(findTestObject('Solocomerce/btn_OK'), 5)

Mobile.comment('Verifikasi apakah item berhasil masuk ke dalam cart belanja')

Mobile.tap(findTestObject('Solocomerce/Page - Detail Product/btn_Cart'), 5)

text_product_name = Mobile.getText(findTestObject('Solocomerce/Page - Cart Menu/tv_Product Name'), 5)

CustomKeywords.'kms.turing.katalon.plugins.assertj.StringAssert.contains'(text_product_name, item_name, 'Verifikasi apakah nama produk mengandung kata Apple Watch')

