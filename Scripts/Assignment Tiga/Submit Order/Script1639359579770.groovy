import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.comment('Checkout cart belanja')

Mobile.tap(findTestObject('Solocomerce/btn_cart'), 5)

Mobile.tap(findTestObject('Solocomerce/Page - Cart Menu/btn_Checkout'), 5)

Mobile.comment('Isi data diri pengiriman')

Mobile.clearText(findTestObject('Solocomerce/Page - Cart Menu/edt_Name'), 5)

Mobile.setText(findTestObject('Solocomerce/Page - Cart Menu/edt_Name'), name, 5)

Mobile.clearText(findTestObject('Solocomerce/Page - Cart Menu/edt_Email'), 5)

Mobile.setText(findTestObject('Solocomerce/Page - Cart Menu/edt_Email'), email, 5)

Mobile.clearText(findTestObject('Solocomerce/Page - Cart Menu/edt_Phone Number'), 5)

Mobile.setText(findTestObject('Solocomerce/Page - Cart Menu/edt_Phone Number'), phone_number, 5)

Mobile.clearText(findTestObject('Solocomerce/Page - Cart Menu/edt_Address'), 5)

Mobile.setText(findTestObject('Solocomerce/Page - Cart Menu/edt_Address'), address, 5)

Mobile.setText(findTestObject('Solocomerce/Page - Cart Menu/edt_Comment'), comment, 5)

Mobile.tap(findTestObject('Solocomerce/Page - Cart Menu/btn_Summit Order'), 5)

Mobile.tap(findTestObject('Solocomerce/btn_OK'), 5)

Mobile.waitForElementPresent(findTestObject('Solocomerce/btn_OK'), 10)

Mobile.tap(findTestObject('Solocomerce/btn_OK'), 5)

