import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Assignment Satu/Login - CU_001'), [('username') : findTestData('CURA Healthcare Data/Login Data').getValue(
            'Username', 1), ('password') : findTestData('CURA Healthcare Data/Login Data').getValue('Password', 1)], FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByLabel(findTestObject('Page_CURA Healthcare Service/select_Health Facility'), findTestData('CURA Healthcare Data/Login Data').getValue(
        'Health_Facility', 1), true)

WebUI.click(findTestObject('Page_CURA Healthcare Service/input_Apply for hospital readmission'))

WebUI.click(findTestObject('Page_CURA Healthcare Service/input_Medicaid_programs'))

WebUI.setText(findTestObject('Page_CURA Healthcare Service/input_Visit Date'), findTestData('CURA Healthcare Data/Login Data').getValue(
        'Visit_Date', 1))

WebUI.setText(findTestObject('Page_CURA Healthcare Service/textarea_Comment'), findTestData('CURA Healthcare Data/Login Data').getValue(
        'Comment', 1))

WebUI.click(findTestObject('Page_CURA Healthcare Service/button_Book Appointment'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_CURA Healthcare Service/h2_Appointment Confirmation'))

WebUI.closeBrowser()

