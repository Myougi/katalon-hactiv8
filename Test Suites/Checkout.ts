<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Checkout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>b9b9ea7c-01e1-4f00-bdb7-a163ebfc7b11</testSuiteGuid>
   <testCaseLink>
      <guid>1f3e0927-4aab-4f20-9a78-dd5a736961f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignment Tiga/Add Item into Cart</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;Apple watch&quot;</value>
         <variableId>40aaaecd-f3e1-479a-9a67-a82ebf9b2b7e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;1&quot;</value>
         <variableId>23f94092-2288-430d-8dc7-0042b3271ed9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e39c6a2b-74f5-41ad-bd89-45041a488775</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignment Tiga/Submit Order</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;Yogi&quot;</value>
         <variableId>b2769eef-2c10-43ee-8025-7cfaefe7a04d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;m.yogi@finansia.com&quot;</value>
         <variableId>44ba10d8-71ad-4b5d-938e-7beab74cbb5c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;087784298239&quot;</value>
         <variableId>62a62fe2-ab05-47b4-b66f-fb0ea722947d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;Head Office&quot;</value>
         <variableId>55858a52-d3b3-4b22-a39e-79fd7782c294</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;No Comment&quot;</value>
         <variableId>845d5bac-1014-4260-b998-2b5b18bb206b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
