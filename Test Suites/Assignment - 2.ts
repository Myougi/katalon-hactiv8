<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>1. Create Order
2. Delete Order</description>
   <name>Assignment - 2</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>cac858a1-b042-4556-9967-0ba8139cf39a</testSuiteGuid>
   <testCaseLink>
      <guid>a7723b97-7050-45d8-86fb-d98f3787cc5e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignment Dua/Create New Post Where User ID Equals 10</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>10</value>
         <variableId>7b52aeb9-eadb-47f2-b7fa-9158d7a2c003</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;Test Title&quot;</value>
         <variableId>0eee2ad6-9edc-4553-b263-c23237302a79</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;Test Body&quot;</value>
         <variableId>80ac6bba-409d-490b-8ae8-2bb51227087e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0d96b8e7-3f1e-465d-90bd-5fd646e291f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignment Dua/GET All Posts Where User ID Equals 10</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ddb04e14-5e8a-48d5-8d8f-04dc39a50227</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4803fcb4-0b83-4df3-8ac5-c57386e3e3ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignment Dua/Delete Post Where Post ID Equals Post ID From Create Post Api</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e0562487-9812-43ce-afd4-44a7412a01a8</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
