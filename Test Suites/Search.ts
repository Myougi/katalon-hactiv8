<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Search</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>4f4d2174-7901-4fa1-8fee-16b00a8d408b</testSuiteGuid>
   <testCaseLink>
      <guid>9103880d-b027-4b5b-958a-9dce689d7d32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignment Tiga/Search Produk</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;Apple&quot;</value>
         <variableId>9bb066bd-8b9e-423c-bdcc-c2b890a95a2a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9142135b-3a02-4265-aaac-464109e2a99b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignment Tiga/Search Category</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;Gadgets&quot;</value>
         <variableId>b4034feb-055c-4168-9571-98ff71e370b2</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
