# Katalon-Hactiv8

Hactiv8 Katalon Training 2021

Repo : https://gitlab.com/Myougi/katalon-hactiv8.git

Tugas Assignment 1 : Web Testing
- Melakukan testing positive dan nevative case dari website cura healthcare

Tugas Assignment 2 : Api Testing
- Create New Post Where User ID Equals 10
- GET All Posts Where User ID Equals 10
- Delete Post Where Post ID Equals Post ID From Create Post Api

Tugas Assignment 3 : Mobile Testing
Test Suite 1 : Search
- Melakukan unit test search produk dan category

Test Suite 2 : Profile
- Melakukan edit nama, email, no telp, dan alamat profile

Test Suite 3 : Checkout
- Menambahkan item yang ingin dibeli
- Melakukan proses submit order pembelian barang